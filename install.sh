#! /bin/bash

# $Id: install.sh 44 2003-02-01 22:56:20Z aqua $

# Edit the following to suit your system

PREFIX=/usr/local/
PERL=`which perl`
SED=`which sed`

CONFDIR=/etc/sugarplum
LOGFILE=/var/log/sugarplum.log
BINDIR=$PREFIX/bin
CGIDIR=/home/httpd/cgi-bin

WEBGROUP=root

# The rest shouldn't need editing, but do so anyway if you feel the need

for dir in $PREFIX $PREFIX/bin $PREFIX/etc $CGIDIR ; do
    if [ ! -d $dir ] ; then
	echo $dir is not a directory, please edit $0
	exit
    fi
done

if [ ! -e "$PERL" ] ; then
    echo Can\'t find perl at $PERL, please edit $0
    exit
fi
if [ ! -e "$SED" ] ; then
    echo Can\'t find sed at $SED, please edit $0
    exit
fi

if ( ! grep -q "^$WEBGROUP:" /etc/group ) ; then
	echo "Group $WEBGROUP does not exist, please edit $0"
	exit
fi

if [ ! -d "$CONFDIR" ] ; then
    install -m 0750 -d $CONFDIR
    install -m 0710 -d $CONFDIR/attacks
fi

if [ ! -e "$LOGFILE" ] ; then
    touch "$LOGFILE"
    chgrp $WEBGROUP "$LOGFILE"
    chmod 0620 "$LOGFILE"
fi

if [ ! -d "$CGIDIR/sugarplum" ] ; then
    install -m 0755 -d $CGIDIR/sugarplum
fi

if [ "X$PERL" = "X/usr/bin/perl" ] ; then
    install -m 0755 poison $CGIDIR/sugarplum/
else
    $SED "s:^#\!/usr/bin/perl:#\!$PERL:" < poison > $CGIDIR/sugarplum/poison
    chmod 0755 $CGIDIR/sugarplum/poison
fi

