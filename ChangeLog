v0.9.10, Tue Apr  1 03:51:36 PST 2003
	- Revised the seeding of deterministic mode.  Added seeding based
	on hostname, to vary sugarplum instances installed on identical
	paths.  Added seeding based on HTTP_HOST for variance across
	different virtualhosts, suggested by Robert Loomans <bts-sugarplum
	at zots.net>.
	- The "cowardly refusal to go 1.0" release

v0.9.9, Sat Feb  1 14:57:43 PST 2003

	- Minor bugfix release; comments from the spammerlist (or 
	the dictionary, if any happened to be there) were being used as valid
	address lines.  Sugarplum now reseeks when it finds such a line.
	Reported by Eric Bischoff <e.bischoff/at/noos.fr>.

v0.9.8, Fri Sep 27 04:07:38 PDT 2002

	- Major revisions after an extended dormancy, during which time
	various spammer tactics had gone out of fashion.

	- Removed DoS counterattack facility removed.  All significant
	operating systems now have sufficiently robust IP stacks as to
	make this pointless.

	- Cycling agent detection removed, since without counterattacks
	it's no longer useful.

	- GDBM dictionaries are no longer used -- flat dictionary and
	spammer lists are now obtained by way of random seeks within
	their original files.  This saves space, is more maintainable,
	and according to my benchmarks is slightly faster.

	- The poison CGI is now configured by way of an external config
	file, not by editing the code itself.

	- All configuration options may now be adjusted on the commandline.

	- Sample configurations no longer match "Teleport" as a harvester;
	Teleport and Teleport Pro are now giving the impression of legitimate
	offline cache bots.

	- Teergrube addressing is now the default.  This is a response to
	criticism of random address generation's potential to collide with
	legitimate addresses.  Random address generation is still available,
	but disabled in the interest of producing a "safe by default" install.

	- Deterministic mode is now the default.

	- The ratio of addresses to normal words has been decreased slightly
	(0.02-0.98 vs 0.05-0.95).

v0.8.4, Thu Dec 28 03:19:24 PST 2000

	- Added teergrube baiting.  Sugarplum can now generate addresses
	for a teergrube (tarpit) or other deliberately-provided spam
	target.  The IP address of the address harvester is encoded into
	the "bait" addresses.

	- Added "deterministic" mode, where the Perl RNG is seeded with a
	number computed from the PATH_INFO variable -- hence the same URL,
	requested twice, will generate the same output.  This makes the
	poison difficult to detect by hitting a URL twice and comparing the
	output.  Suggested by Dominique Quatravaux <dom/at/kilimandjaro.
	dyndns.org>, though this is a tiny crude version of her much better
	suggestion.  This will only work assuming a deterministic RNG.

	- A few output bugfixes.  Thanks to several Slashdot readers who
	observed occasional double-link tags on the recursive links.

	- The frequency of conjunctions has been pushed up by 20% of absolute
	content, though some refinement should be done (e.g. the word 'and' is
	much more common in real speech than 'either').

	- Added WN (web) Server configuration instructions contributed by Jasper
	Jongmans <jjongmans/at/aprogas.cx>

v0.8.3, Wed Nov 22 13:20:47 PST 2000

	- Added the Last-Modified header computation contributed by Eric
	Eisenhart <sugarplum/at\eisenhart.com>.  This helps conceal that
	the output is CGI-generated, and should help also with legitimate
	spiders with broken robots.txt parsing who wander in and then try
	to keep the poion up to date.

	- Added code contributed by Richard Balint <richar.balint@\
	notes.udayton.edu> to generate usernames from dictionary words,
	sometimes with appended numbers.  Such usernames will appear in
	addresses a configurable portion of the time.

	- Added doctype and <html> tags appearing randomly.  Also by
	Richard Balint, with some expansion on the theme.

	- Elevated conjunction frequency.

v0.8.2, Fri Jun  4 16:15:34 PDT 1999

	- Fixed bug where mailto: links from word() were sometimes being
	re-linked as href links by paragraph(); thanks to Alexander
	Kourakos <Alexander@Kourakos.com> for the rpeort (and patch)

	- Added additional document components, all appearing randomly;
	<body> tag with background/text colors, <head> section with
	variable contents including robots, keyword, description meta
	headings.

	- Fixed bug wherein leading <h1> would occasionally be much
	longer than expected.

	- Added distributors of the CherryPicker spambot software to
	the sample-spammers file.

	- Added Teleport-28 to apache rewrite rules and known-spambot
	DoS patterns


	
v0.8, Tue Jun  1 17:22:46 PDT 1999

	- Initial release.

